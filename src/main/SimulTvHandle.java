package main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SimulTvHandle {

	public static WebElement email;

	public static WebElement pass;

	public static WebElement signin;

	public static WebElement memail;

	public static WebElement request;

	public static WebElement cancel;

	public static void main(String args[]) throws Throwable {

		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		WebDriver driver = new ChromeDriver();
		String baseURL = "http://prod.simultv.enveu.com";
		driver.manage().window().maximize();
		driver.get(baseURL);

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("pravinsingh@gmail.com");
		pass.sendKeys("veryeasy1");
		signin.click();
		Thread.sleep(3000);
		email.clear();
		pass.clear();

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("");
		pass.sendKeys("");
		signin.click();
		Thread.sleep(3000);
		email.clear();
		pass.clear();

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("simultvdemo@gmail.com");
		pass.sendKeys("veryeasy1");
		signin.click();
		Thread.sleep(3000);

		driver.findElement(By.linkText("Library Explorer")).click();
		driver.findElement(By.linkText("All Videos")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.linkText("Channel Explorer")).click();
		driver.findElement(By.linkText("All Channels")).click();
		Thread.sleep(3000);

		driver.findElement(By.linkText("Slots Management")).click();
		driver.findElement(By.linkText("Ad Slot Durations")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.linkText("Ad Slot Bookings")).click();
		Thread.sleep(3000);
		
//		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/div/div/div[1]/div[1]/button")).click();
//		Thread.sleep(5000);
		
//		driver.findElement(By.linkText("Slots Management")).click();		
		driver.findElement(By.linkText("Pending For Approval")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.linkText("User Management")).click();
		driver.findElement(By.linkText("Manage Users")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("No")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Yes")).click();
		Thread.sleep(3000);

		driver.findElement(By.linkText("Forgot Password ?")).click();
		memail = driver.findElement(By.xpath("//*[@id='m_email']"));
		request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		memail.sendKeys("");
		request.click();
		Thread.sleep(3000);
		memail.clear();

		memail = driver.findElement(By.xpath("//*[@id='m_email']"));
		memail.sendKeys("pravintest123@gmail.com");
		request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		request.click();
		Thread.sleep(3000);
		memail.clear();

		memail = driver.findElement(By.xpath("//*[@id='m_email']"));
		memail.sendKeys("simultvbrand@gmail.com");
		cancel = driver.findElement(By.xpath("//*[@id='m_login_forget_password_cancel']"));
		cancel.click();
		Thread.sleep(3000);

		driver.findElement(By.id("m_login_forget_password")).click();
		memail = driver.findElement(By.xpath("//*[@id='m_email']"));
		request = driver.findElement(By.xpath("//*[@id='m_login_forget_password_submit']"));
		request.click();
		Thread.sleep(5000);

		driver.close();
	}

}
